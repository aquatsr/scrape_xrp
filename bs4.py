#!/usr/bin/python

activate_this = "/home/tsrawat/Documents/virtenv/scrape_bitstamp/bin/activate_this.py"
execfile(activate_this, dict(__file__=activate_this))

import config
import datetime
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import mongoengine
import requests
import smtplib

DEBUG = False
DB_NAME = config.DB_NAME
class XRP(mongoengine.Document):
    created = mongoengine.DateTimeField(default=datetime.datetime.now)
    price = mongoengine.StringField()


mongoengine.connect(DB_NAME)

URL = 'https://www.bitstamp.net/api/v2/ticker/xrpusd/'

r = requests.get(URL)

xrp_usd = r.json()['last']

new_xrp = XRP()
new_xrp.price = str(xrp_usd)
new_xrp.save()

prices = XRP.objects().order_by('-created')[0:24]

msg_body = ''
msg_subject = ''
list_prices = [float(x.price) for x in prices]
xrp_min = min(list_prices) * 1.0
xrp_max = max(list_prices) * 1.0
xrp_avg = sum(list_prices) / len(list_prices) * 1.0
pct_chg = ((float(new_xrp.price) - xrp_avg)/xrp_avg) * 100
SEND_EMAIL = True
if xrp_min < 0.24000:
    msg_subject = "Bitstamp XRP Stop Loss Triggered"
    msg_body = "Price has fallen below 0.24000\nhttps://aquatsr.com/xrp"
elif pct_chg > 5.0:
    msg_subject = "Bitstamp XRP Sell Recommendation"
    msg_body = "The price of XRP has increased more than 5% relative to the 24h average ({0:.5f}): {1:.1f}%".format(xrp_avg, pct_chg)
elif pct_chg < -5.0:
    msg_subject = "Bitstamp XRP Buy Recommendation"
    msg_body = "The price of XRP has decreased more than 5% relative to the 24h average ({0:.5f}): {1:.1f}%".format(xrp_avg, pct_chg)
else:
    SEND_EMAIL = False

if SEND_EMAIL:
    msg = MIMEMultipart()
    msg['From'] = config.FROM_ADDR
    msg['To'] = config.TO_ADDR
    msg['Subject'] = msg_subject
    msg.attach(MIMEText(msg_body, 'plain'))

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(config.ADDR2, config.ADDR2_PW)

    text = msg.as_string()

    server.sendmail(config.FROM_ADDR, config.TO_ADDR, text)
    server.quit()

if DEBUG:
    xrp_prices = XRP.objects().order_by('-created')
    for xrp in xrp_prices:
        print xrp.price
